#!/usr/bin/env ruby

# Hayden Ball
# 17/03/2016

# This is horrible. But it seems to work.

def convert_hex_to_binary(location, pad = 8)
  location.to_i(16).to_s(2).rjust(pad, '0')
end

def read_from_memory(register, memory_location)
  instr = register == :a ? '00000000' : '00000001'

  location = convert_hex_to_binary(memory_location)

  return instr + " // Copy from #{memory_location} to register #{register.upcase}\n" +
    location
end

def write_to_memory(register, memory_location)
  instr = register == :a ? '00000010' : '00000011'

  location = convert_hex_to_binary(memory_location)

  return instr + " // Write from register #{register.upcase} to #{memory_location}\n" +
    location
end

def alu_op(register, op_code)
  instr = register == :a ? '0100' : '0101'

  code = convert_hex_to_binary(op_code, 4)

  return code + instr + " // ALU Op #{op_code} to register #{register.upcase}"
end

def load_immediate(register, value)
  instr = register == :a ? '00001101' : '00001110'

  location = convert_hex_to_binary(value)

  return instr + " // Load Immediate #{value} to register #{register.upcase}\n" +
    location
end

def branch_instr(instr_str, label)
  instr = case instr_str
          when /BREQ/
            '10010110'
          when /BGTQ/
            '10100110'
          when /BLTQ/
            '10110110'
          when /GOTO/
            '00000111'
          when /FUNCTION_CALL/
            '00001001'
          else
            "Unhandled branch instruction #{instr_with_loc}"
          end

  return instr + " // Branch instruction #{instr_str}\n" +
    label
end

@labels = {}

def handle_op(line, output_addr)
  case line
  when /^$/
	# Empty line
	nil
  when /^\/\//
    # Comment
    nil
  when /(\S+):$/
    # Label
    @labels[$1] = output_addr
    nil
  when /A <- \[(\S+)\]/
    read_from_memory(:a, $1)
  when /B <- \[(\S+)\]/
    read_from_memory(:b, $1)
  when /\[(\S+)\] <- A/
    write_to_memory(:a, $1)
  when /\[(\S+)\] <- B/
    write_to_memory(:b, $1)
  when /A <- ALU_OP (\S+)/
    alu_op(:a, $1)
  when /B <- ALU_OP (\S+)/
    alu_op(:b, $1)
  when /(BREQ|BGTQ|BLTQ|GOTO|FUNCTION_CALL) (\S+)$/
    return branch_instr($1, $2)
  when /GOTO_IDLE/
    '00001000 // GOTO IDLE'
  when /RETURN/
    '00001010 // RETURN'
  when /DEREFERENCE A/
    '00001011 // Dereference A'
  when /DEREFERENCE B/
    '00001100 // Dereference B'
  when /A <- (\S+)/
	load_immediate(:a, $1)
  when /B <- (\S+)/
	load_immediate(:b, $1)
  else
    "Unknown instruction #{line}"
  end
end

input = File.read(ARGV.first)

# Loop through and build basic instructions.
# Where labels are found, add them to a map of label: line number
# We'll do a second pass shortly to fix them

output = ''

input.split("\n").each do |line|
  op_gen = handle_op(line, output.split("\n").length)
  output += op_gen + "\n" if op_gen
end

# Now go through and replace labels
@labels.each do |label, location|
  output.gsub!("#{label}\n", location.to_s(2).rjust(8, '0') + " // " + label + "\n")
end

puts output
